use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Conversation {
    pub logs: String,
}

pub struct ConversationApp {
    pub initialized: bool,
    pub conversations: Vec<(String, Vec<Conversation>)>,
    pub is_conversations_enabled: bool,
    pub openai_prompt_header: String,
    pub is_openai_enabled: bool,
}
mod types;

use std::fs::{File, Permissions};
use std::os::unix::fs::PermissionsExt;
use std::process::Command;

use ciborium::de::from_reader;
use ciborium::ser::into_writer;
use eframe::epaint::text::FontDefinitions;
use eframe::epaint::FontFamily;
use eframe::{Frame, NativeOptions};
use egui::text::{CCursor, CCursorRange};
use egui::{ComboBox, Context, FontData, Key, SidePanel, TextEdit, Ui, Window};
use crate::types::{Conversation, ConversationApp};

impl ConversationApp {
    fn new() -> Self {
        let file = File::options()
            .create(true)
            .read(true)
            .write(true)
            .open("conversations.data")
            .unwrap();
        print_error!(file.set_permissions(Permissions::from_mode(0o777)));
        let conversations = print_error!(from_reader(&file), Vec::new());
        let header = print_error!(from_reader(&file), String::new());
        let to_return = Self {
            initialized: false,
            conversations,
            is_conversations_enabled: false,
            openai_prompt_header: header,
            is_openai_enabled: false,
        };
        to_return
    }

    fn save_manual(&self) {
        let file = File::options()
            .create(true)
            .write(true)
            .truncate(true)
            .open("conversations.data")
            .unwrap();
        print_error!(file.set_permissions(Permissions::from_mode(0o777)));
        print_error!(into_writer(&self.conversations, &file));
        print_error!(into_writer(&self.openai_prompt_header, &file));
    }

    fn add_font(&mut self, ctx: &Context) {
        let mut def = FontDefinitions::default();
        def.font_data.insert(
            "Nanum Gothic Eco".to_string(),
            FontData::from_static(include_bytes!("../NanumGothicEco.ttf")),
        );
        def.font_data.insert(
            "DejaVu Sans".to_string(),
            FontData::from_static(include_bytes!("../DejaVuSans.ttf")),
        );
        def.families
            .get_mut(&FontFamily::Proportional)
            .unwrap()
            .push("DejaVu Sans".to_string());
        def.families
            .get_mut(&FontFamily::Proportional)
            .unwrap()
            .push("Nanum Gothic Eco".to_string());
        ctx.set_fonts(def);
        self.initialized = true;
    }

    fn openai_window(&mut self, ctx: &Context) {
        Window::new("OpenAI interface").show(ctx, |ui| {
            let header = &mut self.openai_prompt_header;

            ui.horizontal(|ui| {
                ui.label("prompt header: ");
                ui.text_edit_multiline(header);
            });

            if self.conversations.is_empty() {
                return;
            }

            use_ui_data!(
                ui,
                selected,
                String,
                self.conversations.first().unwrap().0.clone(),
                {
                    if tuple_get_key(&self.conversations, &selected).is_none() {
                        selected = self.conversations.first().unwrap().0.clone();
                    }
                    ComboBox::from_label("대화 그룹을 선택하세요")
                        .selected_text(&selected)
                        .show_ui(ui, |ui| {
                            self.conversations.iter().for_each(|(group, _)| {
                                ui.selectable_value(&mut selected, group.clone(), group);
                            });
                        });
                }
            );
        });
    }

    fn show_conversations_window(&mut self, ctx: &Context) {
        let conversation_manager = Window::new("Conversation manager")
            .resizable(true)
            .default_height(64.0)
            .default_width(1024.0)
            .min_width(64.0);
        conversation_manager.show(ctx, |ui| 'p: {
            let left_panel = SidePanel::left("Conversation Panel")
                .resizable(true)
                .min_width(16.0)
                .default_width(64.0)
                .max_width(256.0);
            left_panel.show_inside(ui, |ui| {
                use_ui_data!(ui, selected, String, Default::default(), 's: {
                    ui.horizontal(|ui| 'h: {
                        let add = ui.button("+");
                        use_ui_data!(ui, add_name, String, Default::default(), {
                            ui.text_edit_singleline(&mut add_name);
                            if !add_name.is_empty()
                                && add.clicked()
                                && tuple_get_key(&self.conversations, &add_name).is_none()
                            {
                                self.conversations.push((add_name.clone(), Vec::new()))
                            }
                        });
                        let remove = ui.button("-");
                        if !remove.clicked() { break 'h; };

                        let Some(index) = self.conversations
                            .iter()
                            .position(|c| c.0.eq(&selected))
                        else { break 'h; };

                        self.conversations.remove(index);
                    });
                    if self.conversations.is_empty() {
                        break 's;
                    }

                    if tuple_get_key(&self.conversations, &selected).is_none() {
                        selected = self.conversations.first().unwrap().0.clone();
                    }
                    self.conversations.iter().for_each(|(group, _)| {
                        ui.radio_value(&mut selected, group.clone(), group);
                    })
                });
            });

            if self.conversations.is_empty() {
                break 'p;
            }

            use_ui_data!(ui, selected, String, Default::default(), {
                if tuple_get_key(&self.conversations, &selected).is_none() {
                    selected = self.conversations.first().unwrap().0.clone();
                }

                let conversations = tuple_get_key_mut(&mut self.conversations, &selected).unwrap();
                if ui.button("+").clicked() {
                    conversations.push(Conversation {
                        logs: "".to_string(),
                    })
                }

                use_ui_data!(ui, index, usize, 0usize, 's: {
                    if index >= conversations.len() {
                        index = 0;
                        break 's;
                    }

                    ComboBox::from_label("대화 번호를 선택하세요")
                        .selected_text(index.to_string())
                        .show_ui(ui, |ui| {
                            for i in 0..conversations.len() {
                                ui.selectable_value(&mut index, i, i.to_string());
                            }
                        });

                    if ui.button("-").clicked() {
                        conversations.remove(index);
                    }

                    let current = &mut conversations[index].logs;
                    let id = ui.make_persistent_id("conversation editor");
                    let mut modified = false;

                    ctx.input(|input| 'r: {
                        if !input.modifiers.ctrl {
                            break 'r;
                        };
                        if input.key_pressed(Key::T) {
                            current.push_str("tmvkrpxl0: ");
                            modified = true;
                        }
                        if input.key_pressed(Key::K) {
                            current.push_str("kedete: ");
                            modified = true;
                        }
                        if input.key_pressed(Key::S) {
                            current.push_str("system: ");
                            modified = true;
                        }
                        if input.key_pressed(Key::I) {
                            current.push_str("input: ");
                            modified = true;
                        }
                        if input.key_pressed(Key::R) {
                            let Some(last) = current.lines().last() else { break 'r; };
                            let Some(mut start) = last.find("<zsh:") else { break 'r; };
                            let Some(mut end) = last[start..].find('>') else { break 'r; };
                            end += start;
                            start += 5;
                            let command = &last[start..end];
                            let result = match Command::new("sh").arg("-c").arg(command).output() {
                                Ok(o) => {
                                    let mut stdout = String::from_utf8(o.stdout).unwrap();
                                    let stderr = String::from_utf8(o.stderr).unwrap();
                                    if !stderr.is_empty() {
                                        if !stdout.is_empty() {
                                            stdout.push('\n');
                                        }
                                        stdout.push_str(&stderr);
                                    };
                                    stdout.replace('\n', "\\n")
                                }
                                Err(_) => {
                                    break 'r;
                                }
                            };

                            current.push_str("input: ");
                            current.push_str(&result);
                            modified = true;
                        };
                    });

                    if modified {
                        let new_range = CCursorRange::one(CCursor::new(current.len()));
                        let Some(mut state) = TextEdit::load_state(ctx, id) else { break 's; };
                        state.set_ccursor_range(Some(new_range));
                        TextEdit::store_state(ctx, id, state);
                    }

                    TextEdit::multiline(current).id(id).show(ui);
                });
            });
        });
    }

    fn export_json(&self) {
        let file = print_error!(File::options()
            .create(true)
            .write(true)
            .truncate(true)
            .open("./conversations.json"), return);

    }
}

impl eframe::App for ConversationApp {
    fn update(&mut self, ctx: &Context, _frame: &mut Frame) {
        if !self.initialized {
            self.add_font(ctx);
        }

        SidePanel::left("menu").resizable(true).width_range(16.0..=128.0).show(ctx, |ui| {
            self.is_openai_enabled ^= &ui
                .selectable_label(self.is_openai_enabled, "openai")
                .clicked();
            self.is_conversations_enabled ^= &ui
                .selectable_label(self.is_conversations_enabled, "conversation")
                .clicked();

            if ui.button("save").clicked() {
                self.save_manual();
            }
            if ui.button("export").clicked() {
                self.export_json();
            }

            if self.is_openai_enabled {
                self.openai_window(ctx);
            }

            if self.is_conversations_enabled {
                self.show_conversations_window(ctx);
            }
        });
    }

    fn on_close_event(&mut self) -> bool {
        self.save_manual();
        true
    }
}

fn main() {
    eframe::run_native(
        "conversation manager",
        NativeOptions::default(),
        Box::new(|_cc| Box::new(ConversationApp::new())),
    )
    .unwrap();
}

#[macro_export]
macro_rules! print_error {
    ($result:expr, $fallback:expr) => {
        match $result {
            Ok(r) => r,
            Err(e) => {
                eprintln!("{}", e);
                $fallback
            }
        }
    };
    ($result:expr) => {
        match $result {
            Ok(r) => r,
            Err(e) => {
                eprintln!("{}", e);
            }
        }
    };
}

fn tuple_get_key<'a, K: Eq, V>(list: &'a Vec<(K, V)>, key: &K) -> Option<&'a V> {
    list.iter().find(|item| item.0.eq(key)).map(|item| &item.1)
}

fn tuple_get_key_mut<'a, K: Eq, V>(list: &'a mut Vec<(K, V)>, key: &K) -> Option<&'a mut V> {
    list.iter_mut()
        .find(|item| item.0.eq(key))
        .map(|item| &mut item.1)
}

#[macro_export]
macro_rules! use_ui_data {
    ($ui:expr, $id:ident, $t:ty, $insert:expr, $process:expr) => {
        let id = stringify!($id).into();
        let mut $id = $ui.data_mut(|data| {
            let mutable: &mut $t = data.get_temp_mut_or_insert_with(id, || $insert);
            mutable.clone()
        });
        $process
        $ui.data_mut(|data| data.insert_temp(id, $id));
    };
}
